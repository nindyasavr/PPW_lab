# from django.test import TestCase
# from django.test import TestCase, Client, LiveServerTestCase
# from django.urls import resolve
# from .views import story_8
# from selenium import webdriver
# from selenium.webdriver.chrome.options import Options
# from selenium.webdriver.common.keys import Keys
# import selenium.webdriver.chrome.service as service
# import time

# # Create your tests here.
# class Story8UnitTest(TestCase):
# 	def test_story_exist(self):
# 		response = Client().get('/story8/')
# 		#print("Status : " + response.status_code)
# 		self.assertEqual(response.status_code,200)

# 	def test_story8_using_template_html(self):
# 		response = Client().get('/story8/')
# 		self.assertTemplateUsed(response, 'story_8.html')

# 	def test_story8_using_status_func(self):
# 		found = resolve('/story8/')
# 		self.assertEqual(found.func, story_8)
		
# class story8FunctionalTest(LiveServerTestCase):
# 	def setUp(self):
# 		chrome_options = Options()
# 		chrome_options.add_argument('--dns-prefetch-disable')
# 		chrome_options.add_argument('--no-sandbox')
# 		chrome_options.add_argument('--headless')
# 		chrome_options.add_argument('disable-gpu')
# 		self.selenium  = webdriver.Chrome('./chromedriver', chrome_options=chrome_options) 
# 		super(story8FunctionalTest,self).setUp()

# 	def tearDown(self):
#  		self.selenium.quit()
#  		super(story8FunctionalTest, self).tearDown()
		
# 	def test_css_selector(self):
# 		selenium = self.selenium
# 		selenium.get(self.live_server_url)
# 		dropdown_2 = selenium.find_element_by_tag_name('body')
# 		background = dropdown_2.value_of_css_property('background-color')
# 		self.assertEqual(background, 'rgba(173, 216, 230, 1)')