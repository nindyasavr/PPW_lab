from django.shortcuts import render
from django.http import JsonResponse
import requests
from django.http import HttpResponse, HttpResponseRedirect
from django.contrib.auth import authenticate, login, logout
import json


response = {}
# Create your views here.
def login(request):
	if request.user.is_authenticated:
		request.session['user'] = request.user.username
		request.session['email'] = request.user.email

		request.session.get('counter', 0)
		print(dict(request.session))
		for key, value in request.session.items():
			print('{} => {}'.format(key, value))
	return render(request, 'login.html')

def loggingOut(request):
	request.session.flush()
	logout(request)
	return HttpResponseRedirect('/story9')


def story9(request):
	return render(request, 'book_page.html')

def data(request):
	jsonInput = requests.get('https://www.googleapis.com/books/v1/volumes?q=quilting').json()
	# json_parse = json.dumps(json_read)
	if request.user.is_authenticated:
		count = request.session.get('counter',0)
		request.session['counter'] = count
		for key, value in request.session.items():
			print('{} => {}'.format(key, value))
	return JsonResponse(jsonInput)

def increment(request):
	print(dict(request.session))
	request.session['counter'] = request.session['counter'] + 1
	return HttpResponse(request.session['counter'], content_type = "application/json")

def decrement(request):
	request.session['counter'] = request.session['counter'] - 1
	return HttpResponse(request.session['counter'], content_type = "application/json")



