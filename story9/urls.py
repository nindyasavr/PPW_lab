from django.urls import re_path, path
from django.conf.urls import url, include
from .views import story9
from .views import data
from django.contrib.auth import views
from .views import login, loggingOut, increment, decrement


urlpatterns = [
	path('', story9, name='story9'),
	path(r'data', data, name='data'),
	url(r'^login/$', login, name='login'),
    url(r'^logout/$', loggingOut, name='loggingOut'),
    url(r'^auth/', include('social_django.urls', namespace='social')),  # <- Here
    url(r'^o/', include('oauth2_provider.urls', namespace='oauth2_provider')),
	path(r'berkurang', decrement, name='decrement'),
	path(r'nambah', increment, name='increment'),
]
