from django.test import TestCase, Client, LiveServerTestCase
from .views import story9
from django.urls import resolve, reverse
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.keys import Keys
import selenium.webdriver.chrome.service as service
import time


class Story9UnitTest(TestCase):
	"""docstring for Lab4UnitTest"""

	def test_story9_exist(self):
		response = Client().get('/story9/')
		#print("Status : " + response.status_code)
		self.assertEqual(response.status_code,200)

	def test_story9_using_template_html(self):
		response = Client().get('/story9/')
		self.assertTemplateUsed(response, 'book_page.html')

	def test_story9_using_status_func(self):
		found = resolve('/story9/')
		self.assertEqual(found.func, story9)

	def test_ada_your_book(self):
		response = Client().get('/story9/')
		html_response = response.content.decode('utf8')
		self.assertIn('Your Book', html_response)

	# def test_login_admin(self):
	# 	self.failUnlessEqual(self.client.login(username='admin',password='admin'),True)
	# 	self.client.logout()
	# 	response=self.client.get("story9/auth/login/google-oauth2/")
	# 	self.assertRedirects(response,'/signin/oauth/oauthchooseaccount?')

	# def test_ajax_calls(self) :
	# 	response = self.client.get(reverse('/story9'), {}, HTTP_X_REQUESTED='XMLHttpRequest')
	# 	self.assertEquals(response.status_code, 200)

class story9FunctionalTest(LiveServerTestCase):
	def setUp(self):
		chrome_options = Options()
		chrome_options.add_argument('--dns-prefetch-disable')
		chrome_options.add_argument('--no-sandbox')
		chrome_options.add_argument('--headless')
		chrome_options.add_argument('disable-gpu')
		self.selenium  = webdriver.Chrome('./chromedriver', chrome_options=chrome_options) 
		super(story9FunctionalTest,self).setUp()

	def tearDown(self):
 		self.selenium.quit()
 		super(story9FunctionalTest, self).tearDown()
		
	def test_css_selector(self):
		selenium = self.selenium
		selenium.get(self.live_server_url)
		dropdown_2 = selenium.find_element_by_tag_name('body')
		background = dropdown_2.value_of_css_property('background-color')
		self.assertEqual(background, 'rgba(173, 216, 230, 1)')