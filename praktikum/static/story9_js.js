$(document).ready(function() {
	$.ajax({
		url: "data",
		datatype: 'json',
		success: function(data){
			var result ='<tr>';
			for(var i = 0; i < data.items.length; i++) {
				result += "<td scope='row' class='align-middle text-center'>" + (i+1) + "</td>" +
				"<td class='align-middle'>" + data.items[i].volumeInfo.title +"</td>" +
				"<td class='align-middle'>" + data.items[i].volumeInfo.authors + "</td>" + 
				"<td class='align-middle'>" + data.items[i].volumeInfo.publisher +"</td>" + 
				"<td><img class='img-fluid' style='width:22vh' src='" + data.items[i].volumeInfo.imageLinks.smallThumbnail +"'></img>" + "</td>" +
				"<td class='align-middle' style='text-align:center'>" + "<img id='bintang" + i + "' onclick='favorite(this.id)' width='28px' src='https://image.flaticon.com/icons/svg/263/263075.svg'>" + "</td></tr>";
			}
			$('tbody').append(result);
		}
	})
});

var counter = 0;
function favorite(clicked_id){
	var button = document.getElementById(clicked_id);
	if(button.classList.contains("checked")){
		button.classList.remove("checked");
		document.getElementById(clicked_id).src = 'https://image.flaticon.com/icons/svg/263/263075.svg';
		$.ajax({
			url: "decrement",
			dataType: 'json',
			success: function(result){
				var counter = JSON.parse(result);
				console.log(result)
				document.getElementById("counter").innerHTML = counter;
				$('#counter').html(counter);
			}
		});
	}
	else{
		button.classList.add('checked');
		document.getElementById(clicked_id).src = 'https://image.flaticon.com/icons/svg/448/448014.svg';
		$.ajax({
			url: "increment",
			dataType: 'json',
			success: function(result){
				var counter = JSON.parse(result);
				console.log(result)
				document.getElementById("counter").innerHTML = counter;
				$('#counter').html(counter);
			}
		});

	}
}

