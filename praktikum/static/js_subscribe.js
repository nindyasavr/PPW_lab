$(document).ready(function() {
    var nama = "";
    var password = "";
    var emailIsValid = false;
    $("#id_nama").change(function() {
        nama = $(this).val();
    });
    $("#id_password").keyup(function() {
        password = $(this).val();
        console.log(emailIsValid);
        if(nama.length > 0 && password.length > 7 && emailIsValid) {
            $("#button-button-2").prop("disabled", false);
        }
    })
    $("#id_email").change(function() {
        console.log("HORE");
        var email = $(this).val();
        $.ajax({
            url: "/valid/",
            data: {
                'email': email,
            },
            type: "POST",
            dataType: 'json',
            success: function(data) {
                console.log(data['not_valid']);
                if(data['not_valid'] == true) {
                    alert("Invalid Email! Mohon maaf Email sudah terdaftar");
                } else {
                    emailIsValid = true;
                    if(nama.length > 0 && password.length > 7) {
                        $("#button-button-2").prop("disabled", false);
                    }
                }
                
            }
        });
    });
    $("#button-button-2").click(function() {
        event.preventDefault();
        $.ajax({
            headers: {"X-CSRFToken": $("input[nama=csrfmiddlewaretoken]").val()},
            url:"/subscribe",
            data: $("form").serialize(),
            type: 'POST',
            dataType: 'json',
            success: function(data){
                alert("SUCCESS!");
                document.getElementById('id_nama').value = '';

                document.getElementById('id_email').value = '';

                document.getElementById('id_password').value = '';

                $("#button-button-2").prop("disabled", true);  

            }
        })
    });
});
