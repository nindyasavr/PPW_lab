"""Lab1 URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import include, url
from django.urls import re_path, path
from django.contrib import admin
from lab_1.views import index as index_lab1
from lab_3.views import index
from lab_3.views import identity
from lab_3.views import education
from lab_3.views import skill
from lab_3.views import form
from lab_3.views import message_post
from lab_3.views import schedule
from lab_3.views import submit, remove_all
from lab_4.views import index
from lab_4.views import add_status
from story9.views import story9
from story9.views import story9




urlpatterns = [
    re_path(r'^admin/', admin.site.urls),
    re_path(r'^lab-1/', include('lab_1.urls')),
    re_path(r'^lab-2/', include('lab_2.urls')),
    re_path(r'^index', index, name='index'),
    re_path(r'^identity', identity, name='identity'),
    re_path(r'^education', education, name='education'),
    re_path(r'^skill', skill, name='skill'),
    re_path(r'^form', form, name='form'),
    re_path(r'^schedule', schedule, name='schedule'),
    re_path(r'^submit', submit, name='submit'),
    re_path(r'^post', message_post, name='message_post'),
    re_path(r'^remove_all', remove_all, name='remove_all'),
    path('lab-4/', include('lab_4.urls'), name="lab_4"),
    path(r'', include('lab_4.urls')),
    path('story8/', include('story8.urls'), name="story8"),
    path('story9/', include('story9.urls'), name="story9"),
    path('story10/', include('story10.urls'), name="story10"),
    url(r'^auth/', include('social_django.urls', namespace='social')),
]
