from django.urls import re_path, path
from .views import story10
from .views import subscribe
from .views import valid

urlpatterns = [
	re_path(r'^$', story10, name='landpage'),
	re_path(r'^subscribe', subscribe, name='subscribe'),
	re_path(r'^valid', valid, name='valid'),


]
