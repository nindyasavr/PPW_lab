from django import forms

class subscribe_form(forms.Form):
    nama = forms.CharField(widget=forms.TextInput())
    email = forms.EmailField(widget=forms.EmailInput())
    password = forms.CharField(widget=forms.PasswordInput(), min_length=8, error_messages={"min_length" : "password is not valid"})