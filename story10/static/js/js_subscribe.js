$(document).ready(function() {
    var nama = "";
    var password = "";
    var emailIsValid = false;
    $("#id_nama").change(function() {
        nama = $(this).val();

    });
    $("#id_password").keyup(function() {
        password = $(this).val();
        console.log(emailIsValid);
        if(nama.length > 0 && emailIsValid) {
            $("#button-button-2").prop("disabled", false);
        }
    })

    $("#id_email").change(function() {
        var email = $(this).val();
        var csrf = $("input[name=csrfmiddlewaretoken]").val();

        $.ajax({
            headers: {"X-CSRFToken": csrf },
            url: "/story10/valid/",
            data: {
                'email': email,
            },
            type: "POST",
            dataType: 'json',
            success: function(data) {
                console.log(data['not_valid']);
                if(data['not_valid'] == true) {
                    alert("Invalid Email! Mohon maaf Email sudah terdaftar");
                } else {
                    emailIsValid = true;
                    if(nama.length > 0 && password.length > 7) {
                        console.log("MASUK 3");
                        $("#button-button-2").prop("disabled", false);
                    }
                }
                
            }
        });
    });
    $("#button-button-2").click(function() {
        event.preventDefault();
        $.ajax({
            headers: {"X-CSRFToken": $("input[name=csrfmiddlewaretoken]").val()},
            url:"/story10/subscribe/",
            data: $("form").serialize(),
            type: 'POST',
            dataType: 'json',
            success: function(data){
                alert("SUCCESS!");
                document.getElementById('id_nama').value = '';

                document.getElementById('id_email').value = '';

                document.getElementById('id_password').value = '';

                $("#button-button-2").prop("disabled", true);  

            }
        })
    });
});
