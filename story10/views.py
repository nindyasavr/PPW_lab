from django.shortcuts import render
from django.urls import reverse
from django.http import HttpResponse, HttpResponseRedirect, JsonResponse
from .models import subscribe_model
from .forms import subscribe_form
from django.views.decorators.csrf import csrf_exempt
import urllib.request, json 
import requests


# Create your views here.
def story10(request):
	response = {'form' : subscribe_form}
	response['subscribe'] = subscribe_form
	return render(request, 'story10.html', response)

def valid(request):
    email = request.POST.get("email")
    exist = subscribe_model.objects.filter(email=email)
    if exist:
        data = {
            'not_valid': True
        }
    else:
        data = {
            'not_valid': False
        }
    return JsonResponse(data)

@csrf_exempt
def subscribe(request):
	form = subscribe_form(request.POST or None)
	if (form.is_valid()):
		datas = form.cleaned_data
		datas = subscribe_model(nama=datas['nama'], email=datas['email'], password=datas['password'])
		datas.save()
		data = {'nama': datas['name'], 'email': datas['email'], 'password': datas['password']}
		return JsonResponse(data)





