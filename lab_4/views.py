from django.shortcuts import render
from .models import StatusHarian
from .forms import DaftarHarian
from django.http import HttpResponse, HttpResponseRedirect


# Create your views here.
response = {}
def index(request):
	response['daftar'] = DaftarHarian
	return render(request, 'status.html', response)


def add_status(request):
	response['daftar'] = DaftarHarian
	# response['daftarStatus'] = StatusHarian.objects.all().values()
	html = "status.html"
	daftarStatus = DaftarHarian(request.POST)
	if request.method == 'POST' :
		response['StatusHari'] = request.POST['StatusHari']
		daftarStatus = StatusHarian(StatusHari=response['StatusHari'])
		daftarStatus.save()
		response['daftarStatus'] = StatusHarian.objects.all()
		html ='status.html'
		return render (request, html, response)
	else:
		return render(request, html, response)

def profile(request):
	return render(request, 'profile.html')
