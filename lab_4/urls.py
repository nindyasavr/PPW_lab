from django.urls import re_path, path
from .views import index
from .views import add_status
from .views import profile

urlpatterns = [
	re_path(r'^add', add_status, name='add_status'),
    re_path(r'^$', index, name='status'),
    re_path(r'^prof', profile, name='profile'),

]
