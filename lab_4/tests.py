# from django.test import TestCase, Client, LiveServerTestCase
# from django.urls import resolve
# from .views import index
# from .views import add_status
# from .models import StatusHarian
# from .views import profile
# from selenium import webdriver
# from selenium.webdriver.chrome.options import Options
# from selenium.webdriver.common.keys import Keys
# import selenium.webdriver.chrome.service as service
# import time

# # Create your tests here.
# class Lab4UnitTest(TestCase):
# 	"""docstring for Lab4UnitTest"""

# 	def test_lab4_exist(self):
# 		response = Client().get('/lab-4/')
# 		#print("Status : " + response.status_code)
# 		self.assertEqual(response.status_code,200)

# 	def test_lab_4_using_template_html(self):
# 		response = Client().get('/lab-4/')
# 		self.assertTemplateUsed(response, 'status.html')

# 	def test_lab_4_using_status_func(self):
# 		found = resolve('/lab-4/')
# 		self.assertEqual(found.func, index)

# 	def test_ada_hello(self):
# 		response = Client().get('/lab-4/')
# 		html_response = response.content.decode('utf8')
# 		# self.assertIn('Hello, apa kabar?', html_response)

# 	def test_model_can_create_status(self):
# 		new_status = StatusHarian.objects.create(StatusHari='Aku hari ini sehat')
# 		status = StatusHarian.objects.all().count()
# 		self.assertEqual(status,1)

# 	def test_prof_exist(self):
# 		response = Client().get('/lab-4/prof/')
# 		#print("Status : " + response.status_code)
# 		self.assertEqual(response.status_code,200)

# class Lab_4FunctionalTest(LiveServerTestCase):
# 	def setUp(self):
# 		chrome_options = Options()
# 		chrome_options.add_argument('--dns-prefetch-disable')
# 		chrome_options.add_argument('--no-sandbox')
# 		chrome_options.add_argument('--headless')
# 		chrome_options.add_argument('disable-gpu')
# 		self.selenium  = webdriver.Chrome('./chromedriver', chrome_options=chrome_options) 
# 		super(Lab_4FunctionalTest,self).setUp()

# 	def test_can_do(self):
# 		selenium = self.selenium
# 		selenium.get(self.live_server_url)
# 		status_box = selenium.find_element_by_name('StatusHari')
# 		submit = selenium.find_element_by_id('submit')
# 		status_box.send_keys('Coba Coba')
# 		submit.send_keys(Keys.RETURN)
# 		time.sleep(5) # Let the user actually see something!
# 		self.assertIn( "Coba Coba", selenium.page_source)

# 	def test_location_status(self):
# 		selenium = self.selenium
# 		selenium.get(self.live_server_url)
# 		status = selenium.find_element_by_name('StatusHari')
# 		size_status = status.size
# 		location_status = status.location
# 		self.assertEqual(size_status,{'height': 206, 'width': 303})
# 		self.assertEqual(location_status, {'x': 50, 'y': 162})

# 	def test_location_submit_button(self):
# 		selenium = self.selenium
# 		selenium.get(self.live_server_url)
# 		submit_box = selenium.find_element_by_id('submit')
# 		size_submit = submit_box.size
# 		location_submit = submit_box.location
# 		self.assertEqual(size_submit,{'height': 34, 'width': 70})
# 		self.assertEqual(location_submit, {'x': 50, 'y': 373})

# 	def test_css_text_profil(self):
# 		selenium = self.selenium
# 		selenium.get(self.live_server_url)
# 		profil = selenium.find_element_by_id('profil')
# 		font = profil.value_of_css_property('font-size')
# 		shadow = profil.value_of_css_property('text-shadow')
# 		self.assertEqual(font, '30px')
# 		self.assertEqual(shadow, 'rgb(255, 255, 0) 2px 2px 5px')

# 	def test_css_body_background(self):
# 		selenium = self.selenium
# 		selenium.get(self.live_server_url)
# 		body = selenium.find_element_by_id('body-status')
# 		background = body.value_of_css_property('background-color')
# 		self.assertEqual(background, 'rgba(173, 216, 230, 1)')

# 	def tearDown(self):
# 		self.selenium.quit()
# 		super(Lab_4FunctionalTest, self).tearDown()