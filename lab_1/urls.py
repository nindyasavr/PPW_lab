from django.urls import re_path
from .views import index
from .views import story3
#url for app
urlpatterns = [
    re_path(r'^$', index, name='index'),
    re_path(r'^story3$', story3, name='story3'),
]
