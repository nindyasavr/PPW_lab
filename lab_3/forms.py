from django import forms

class Message_Form(forms.Form):
    hari = forms.DateField(widget=forms.DateInput())
    waktu = forms.DateTimeField(widget=forms.TimeInput())
    tempat = forms.CharField(widget=forms.Textarea())
    nama_kegiatan = forms.CharField(widget=forms.Textarea())
    kategori = forms.CharField(widget=forms.Textarea())