from django.shortcuts import render
from django.urls import reverse
from datetime import datetime
from .models import JadwalKegiatan
from .forms import Message_Form
from django.http import HttpResponse, HttpResponseRedirect

response = {}
def index(request):
	return render(request, 'story3.html')

def identity(request):
	return render(request, 'identity.html')

def education(request):
	return render(request, 'education.html')

def skill(request):
	return render(request, 'skill.html')

def form(request):
	return render(request, 'form.html')

def submit(request):
    html = "submit.html"
    response["message_form"] = Message_Form
    schedule = JadwalKegiatan.objects.all()
    response['schedule'] = schedule
    return render(request, html, response)
# Create your views here.
def message_post(request):
    form = Message_Form(request.POST or None)
    if(request.method == 'POST' or form.is_valid()):
        response['hari'] = datetime.strptime(request.POST['hari'], '%Y-%m-%d')
        response['waktu'] = datetime.strptime(request.POST['waktu'], '%H:%M')
        response['tempat'] = request.POST['tempat']
        response['nama_kegiatan'] = request.POST['nama_kegiatan']
        response['kategori'] = request.POST['kategori']
        schedule = JadwalKegiatan(hari=response['hari'], waktu=response['waktu'], tempat=response['tempat'], nama_kegiatan=response['nama_kegiatan'], kategori=response['kategori'])
        schedule.save()
        html ='schedule.html'
        response['schedule'] = JadwalKegiatan.objects.all().values()
        return render(request, html, response)

    else:
        return HttpResponseRedirect(reverse('submit'))

def schedule(request):
	schedule = JadwalKegiatan.objects.all()
	response['schedule'] = schedule
	return render(request,"schedule.html", response)

def remove_all(request):
	remove = JadwalKegiatan.objects.all().delete()
	return HttpResponseRedirect(reverse('schedule'))
