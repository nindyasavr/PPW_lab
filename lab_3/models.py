from django.db import models
from django.utils import timezone
from datetime import datetime, date

class JadwalKegiatan(models.Model):
	nama_kegiatan = models.CharField(max_length=100)
	hari = models.DateField()
	waktu = models.DateTimeField()
	tempat = models.CharField(max_length=100)
	kategori = models.CharField(max_length=60)
	 