from django.urls import re_path
from .views import index
from .views import identity
from .views import education
from .views import skill
from .views import form
from .views import message_post
from .views import schedule
from .views import submit, remove_all


#url for app
urlpatterns = [
    re_path(r'^story3$', story3, name='story3'),
    re_path(r'identity', identity, name='identity')
    re_path(r'^education', education, name='education')
    re_path(r'^skill', skill, name='skill')
    re_path(r'^form', form, name='form')
    re_path(r'^schedule', schedule, name='schedule'),
    re_path(r'^submit', submit, name='submit'),
    re_path(r'^post', message_post, name='message_post'),
    re_path(r'^remove_all', remove_all, name='remove_all'),

]
